<?php
  error_reporting(E_ALL);
  ini_set("display_errors", "On");

  function randnumb($length = 3) {
    return substr(str_shuffle(str_repeat($x='0123456789', ceil($length/strlen($x)) )),1,$length);
  }

  $ok = true;
  $num_images = 1;
  $images = [];

  for ($i = 1; $i <= 4; $i++) {
    $error = $_FILES["image_" . $i]['error'];
    if($error == UPLOAD_ERR_OK) {
      array_push($images, $_FILES["image_" . $i]);
    }
  }

  if(count($images) == 0) {
    echo "No image was provided.";
    $ok = false;
  }

  if(count($images) > 4) {
    echo "Too many images were provided.";
    $ok = false;
  }

  for ($i = 0; $i < count($images); $i++) {
    if($images[$i]["size"] > 9999999) {
      echo "File is too large.";
      $ok = false;
    }
  }

  if($ok) {
    $fstring = "";
    $uploaded = [];
    $start = true;

    for ($i = 0; $i < count($images); $i++) {
      $fname = basename($images[$i]["name"]);
      $ext = substr($fname, strrpos($fname, '.') + 1);
      $fname2 = time() . "_" . randnumb() . "_" . $i . "." . $ext;
      $target = "uploads/" . $fname2;

      if(move_uploaded_file($images[$i]["tmp_name"], $target)) {
        array_push($uploaded, $target);
        $fstring = trim($fstring . " " . $target);
      } else {
        echo "There was an error uploading the image(s).";
        $start = false;
        break;
      }
    }

    if($start) {
      process($fstring);
    }

    for ($i = 0; $i < count($uploaded); $i++) {
      unlink($uploaded[$i]);
    }
  }

  function process($filestring) {
    $modes = [];
    $num_effects = 8;
    $delay = 0.25;

    for ($i = 1; $i <= $num_effects; $i++) {
      if(isset($_POST["effect_" . $i])) {
        array_push($modes, $i);
      }
    }

    if(count($modes) == 0) {
      echo "No effects selected.";
      exit(0);
    }

    $modestring = implode(",", $modes);

    if(isset($_POST["delay"])) {
      $delay = $_POST["delay"];
    }

    $date1 = microtime(true);
    $cmd = "bin/mutant " . $modestring . " " . $delay . " " . $filestring;
    $paths = explode(" ", exec($cmd));
    $date2 = microtime(true);

    $style = "body, html {
      background-color: black;
      color: white;
      font-family: sans-serif;
      font-size: 18px;
    } 
      
    .item {
      display: inline-block;
      padding: 20px;
    } 
      
    .title {
      padding-bottom: 5px;
      display: block;
    }
      
    .info {
      margin-left: 20px;
      margin-right: 20px;
      margin-top: 20px;
      margin-bottom: 15px;
      padding: 5px;
      background-color: #32343d
    }";

    $topheader = "<title>Mutant Result</title>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
    <link rel='shortcut icon' href='guy.png?v=1' type='image/x-icon'>";

    $diff = round(($date2 - $date1), 2);
    $seconds = "seconds";

    if($diff == 1) {
      $seconds = "second";
    }

    echo "<head>" . $topheader . "<style>" . $style . "</style></head>";

    $effects = "effects";

    if(count($modes) == 1) {
      $effects = "effect";
    }

    $imgs = "The images";

    if(count($modes) == 1) {
      $imgs = "The image";
    }

    $msg = "&nbsp;&nbsp;|&nbsp;&nbsp;" . $imgs . " will be deleted in 2 days";
    echo "<div class='info'>" . count($modes) . " " . $effects . " processed in " . $diff . " " . $seconds . $msg . "</div>";

    $effect_name_1 = "Glitch";
    $effect_name_2 = "Wave";
    $effect_name_3 = "Mirror";
    $effect_name_4 = "Static";
    $effect_name_5 = "Glow";
    $effect_name_6 = "Glass";
    $effect_name_7 = "Color";
    $effect_name_8 = "Chalk";

    for ($i = 0; $i < count($modes); $i++) {
      echo "<div class='item'><div class='title'>" . ${"effect_name_" . $modes[$i]} . " | Size: " . round(filesize($paths[$i]) / 1024 / 1024, 1) . " MiB</div>";
      echo "<img class='image' src='" . $paths[$i] . "'></div>";
    }
  }
?>